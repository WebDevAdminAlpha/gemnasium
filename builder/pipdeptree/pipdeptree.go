package pipdeptree

import (
	"os"
	"os/exec"
)

// excludedPackages is set so that pipdeptree excludes package managers,
// virtualenv, and pipdeptree itself from the package list
const excludedPackages = "pip,pipenv,pipdeptree,setuptools,wheel,virtualenv,virtualenv-clone"

// CreateJSON saves the JSON output of pipdeptree into a file
func CreateJSON(path string) error {
	cmd := exec.Command("pipdeptree", "--json", "--exclude", excludedPackages)
	cmd.Stderr = os.Stderr

	// create output file
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	cmd.Stdout = f

	// start and wait
	err = cmd.Start()
	if err != nil {
		return err
	}
	return cmd.Wait()
}
