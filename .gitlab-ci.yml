variables:
  REPORT_FILENAME: gl-dependency-scanning-report.json
  DS_EXCLUDED_PATHS: "*.excluded"
  # need to ignore the fixtures dir, otherwise when dependency scanning runs against
  # the gemnasium project, it will end up scanning the test fixtures dir which contains
  # invalid and broken files used by the unit tests
  SEARCH_IGNORED_DIRS:  "fixtures"
  MAJOR: 2
  MAX_IMAGE_SIZE_MB: 130
  MAX_SCAN_DURATION_SECONDS: 1

include:
  - https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/analyzer.yml

npm test:
  image: node:11-alpine
  stage: pre-build
  before_script:
    - cd vrange/npm
  script:
    - yarn install
    - ./test_rangecheck.js

php test:
  image: php:7-alpine
  stage: pre-build
  before_script:
    - cd vrange/php
  script:
    - apk add composer
    - composer install
    - ./vendor/bin/phpunit --bootstrap vendor/autoload.php RangeCheckTest

gem test:
  image: ruby:2.5-alpine
  stage: pre-build
  before_script:
    - cd vrange/gem
  script:
    - bundle install
    - rake test

python test:
  image: python:3-alpine
  stage: pre-build
  before_script:
    - cd vrange/python
  script:
    - pip install -r requirements.txt
    - python -m unittest rangecheck_test.py

gemnasium-dependency_scanning:
  before_script:
    - echo 'removing testdata and fixtures to quickfix https://gitlab.com/gitlab-org/gitlab/-/issues/292693'
    - find . -name fixtures -type d| xargs rm -rf
    - find . -name testdata -type d| xargs rm -rf

dotnet test:
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  stage: pre-build
  before_script:
    - cd vrange/nuget/VrangeTest
  script:
    - dotnet test

.functional:
  extends: .qa-downstream-ds
  variables:
    DS_DEFAULT_ANALYZERS: "gemnasium"
    DS_ANALYZER_IMAGE: "$CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA"

php-composer-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/php-composer/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 3
  trigger:
    project: gitlab-org/security-products/tests/php-composer

ruby-bundler-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/ruby-bundler/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/ruby-bundler

js-npm-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-npm/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 2
  trigger:
    project: gitlab-org/security-products/tests/js-npm

js-npm7-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-npm/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 2
  trigger:
    project: gitlab-org/security-products/tests/js-npm
    branch: lockfile-v2-FREEZE

js-npm-shrinkwrap-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-npm-shrinkwrap/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 2
  trigger:
    project: gitlab-org/security-products/tests/js-npm
    branch: shrinkwrap-FREEZE

js-npm-offline-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-npm-offline/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 2
  trigger:
    project: gitlab-org/security-products/tests/js-npm
    branch: offline-FREEZE

js-yarn-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-yarn/$REPORT_FILENAME"
    DS_REMEDIATE: "false"
  trigger:
    project: gitlab-org/security-products/tests/js-yarn

js-yarn-ds-remediate-top-level-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js-yarn-ds-remediate-top-level/$REPORT_FILENAME"
    DS_REMEDIATE: "true"
  trigger:
    project: gitlab-org/security-products/tests/js-yarn
    branch: ds-remediate-top-level

go-modules-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/go-modules/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/go-modules

go-modules-gemnasium-db-checkout-test:
  extends: .functional
  trigger:
    project: gitlab-org/security-products/tests/go-modules
    branch: advisory-db-scan-time-sync-FREEZE

go-modules-gemnasium-db-update-disabled-test:
  extends: .functional
  variables:
    GEMNASIUM_DB_UPDATE_DISABLED: "true"
  trigger:
    project: gitlab-org/security-products/tests/go-modules
    branch: advisory-db-scan-time-sync-FREEZE

# test DS_EXCLUDED_PATHS to ensure that the given path is ignored and results in no vulnerabilities in the
# expected report
ds-excluded-paths-qa:
  extends: .functional
  variables:
    DS_EXCLUDED_PATHS: "/go.sum"
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/no-vulnerabilities/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/go-modules

csharp-nuget-dotnetcore-qa:
  extends: .functional
  variables:
    DS_DEPENDENCY_PATH_MODE: all
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/csharp-nuget-dotnetcore/$REPORT_FILENAME"
    MAX_SCAN_DURATION_SECONDS: 2
  trigger:
    project: gitlab-org/security-products/tests/csharp-nuget-dotnetcore
    branch: master

c-conan-qa:
  extends: .functional
  variables:
    DS_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/c-conan/$REPORT_FILENAME"
  trigger:
    project: gitlab-org/security-products/tests/c-conan
    branch: master
